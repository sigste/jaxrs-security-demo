package jaxrssecurity;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/msg")
public class RestService {

    /**
     * curl http://localhost:8080/rest/msg
     */
    @GET
    @Produces("text/plain")
    @Path("/free")
    public Response getFree() {
        return Response.status(200).entity("Free for all msg").build();
    }

    /**
     * curl http://localhost:8080/rest/msg/restricted
     */
    @GET
    @Produces("text/plain")
    @Path("/restricted")
    @RolesAllowed("user")
    public Response getRestricted() {
        return Response.status(200).entity("Restricted msg").build();
    }

    /**
     * curl http://localhost:8080/rest/msg/secret
     */
    @GET
    @Produces("text/plain")
    @DenyAll
    @Path("/secret")
    public Response getSecret() {
        return Response.status(200).entity("Secret msg").build();
    }

}