package jaxrssecurity;

import org.glassfish.jersey.internal.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Sigurd Stendal
 * Date: 11.10.12
 */
public class BasicAuthParser {

    protected static Logger logger = LoggerFactory.getLogger(BasicAuthParser.class);

    public static BasicAuthData parseHeader(String header) {
        BasicAuthData data = new BasicAuthData();
        try {
            if (header == null) {
                logger.info("Authorization header field is empty");
                return null;
            }
            if (!header.startsWith("Basic ")) {
                logger.error("Failed while parsing Authorization header field. Authorization header does not start with 'Basic '");
                return null;
            }
            if (header.length() == "Basic ".length()) {
                logger.error("Failed while parsing Authorization header field. Authorization header was only 'Basic '. Service name and password was missing.");
                return null;
            }
            String authorization = Base64.decodeAsString(header.substring("Basic ".length()));
            int i = authorization.indexOf(":");
            if (i == -1) {
                logger.error("Failed while parsing Authorization header field. Base64 encoded data did not contain any ':'");
                return null;
            }
            data.username = authorization.substring(0, i);
            data.password = authorization.substring(data.username.length() + 1);
            return data;
        } catch (Exception e) {
            logger.error("Failed while parsing Authorization header field", e);
            return null;
        }
    }

}

class BasicAuthData {
    String username;
    String password;
}

