package jaxrssecurity;

import java.security.Principal;

/**
 * User: Sigurd Stendal
 * Date: 28.02.14
 */
public class User implements Principal {

    private String username;

    public User(String username) {
        this.username = username;
    }

    @Override
    public String getName() {
        return username;
    }
}
