package jaxrssecurity;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;

/**
 * User: Sigurd Stendal
 * Date: 13.10.13
 */
public class SecurityFilter implements Filter {

    private static final String BASIC_AUTH_DOMAIN = "JAX RS security demo";

    protected Logger logger = LoggerFactory.getLogger(SecurityFilter.class);


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Init database connection etc.
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        logger.debug("Checking username and password");
        BasicAuthData data = BasicAuthParser.parseHeader(request.getHeader("Authorization"));
        if (data != null) {
            if (!data.username.equals(data.password)) { // Password check
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                response.setHeader("WWW-Authenticate", "Basic realm=" + BASIC_AUTH_DOMAIN);
                return;
            }

            request = wrappedHttpServletRequest(request, data.username);
        }

        filterChain.doFilter(request, response);
    }

    private HttpServletRequest wrappedHttpServletRequest(HttpServletRequest request, String username) {

        final User user = new User(username);

        return new HttpServletRequestWrapper(request) {
            @Override
            public Principal getUserPrincipal() {
                return user;
            }

            @Override
            public String getAuthType() {
                return HttpServletRequest.BASIC_AUTH;
            }

            @Override
            public String getRemoteUser() {
                return user.getName();
            }

            @Override
            public boolean isUserInRole(String role) {
                return role.equals("user");
            }

        };
    }

    @Override
    public void destroy() {
        // Nothing
    }

}
