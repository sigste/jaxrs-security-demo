package jaxrssecurity;

import org.glassfish.jersey.server.ResourceConfig;

/**
 * User: Sigurd Stendal
 * Date: 28.02.14
 */
public class SecuredApplication extends ResourceConfig {


    public SecuredApplication() {
        super();
        register(RolesAllowedDynamicFeature.class);
    }
}
